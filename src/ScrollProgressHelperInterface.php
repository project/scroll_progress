<?php

namespace Drupal\scroll_progress;

/**
 * Interface ScrollProgressHelperInterface.
 */
interface ScrollProgressHelperInterface {

  /**
   * Evaluate visibility conditions.
   *
   * @return bool
   *   Return TRUE if visibility conditions match. Otherwise FALSE.
   */
  public function evaluate();

  /**
   * Evaluate the visibility request path conditions.
   *
   * @param array $request_path
   *   The request path conditions.
   *
   * @return bool
   *   Return TRUE if visibility conditions match. Otherwise FALSE.
   */
  public function evaluateRequestPath(array $request_path);

  /**
   * Checks scroll progress is enabled.
   *
   * @return bool
   *   Return TRUE if enabled.
   */
  public function isEnabled();

  /**
   * Gets the module configuration.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The configuration object.
   */
  public function getConfiguration();

  /**
   * Gets the visibility configurations.
   *
   * @return array
   *   The visibility configurations keyed per visibility type.
   */
  public function getVisibility();

}
