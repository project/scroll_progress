<?php

namespace Drupal\scroll_progress;

use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class ScrollProgressHelper.
 */
class ScrollProgressHelper implements ScrollProgressHelperInterface {

  /**
   * Drupal\path_alias\AliasManagerInterface definition.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * Drupal\Core\Path\PathMatcherInterface definition.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Path\CurrentPathStack definition.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ScrollProgressHelper constructor.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $path_alias_manager
   *   The path_alias manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The path current.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(AliasManagerInterface $path_alias_manager, PathMatcherInterface $path_matcher, RequestStack $request_stack, CurrentPathStack $current_path, ConfigFactoryInterface $config_factory) {
    $this->pathAliasManager = $path_alias_manager;
    $this->pathMatcher = $path_matcher;
    $this->requestStack = $request_stack;
    $this->currentPath = $current_path;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $visibility = $this->getVisibility();
    $result = TRUE;
    foreach ($visibility as $visibility_type => $conditions) {
      if ($visibility_type === 'request_path') {
        $result = $this->evaluateRequestPath($conditions);
        // Currently all conditions must pass. May be in a near future we could
        // implement an OR/AND logic and if there is more than the request path
        // conditions visibility.
        if ($result === FALSE) {
          break;
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluateRequestPath(array $request_path) {
    $result = TRUE;
    $negate = !empty($request_path['negate']);
    $pages = !empty($request_path['pages']) ? trim($request_path['pages']) : '';
    if (empty($pages)) {
      return $negate ? !$result : $result;
    }
    // Convert path to lowercase. This allows comparison of the same path
    // with different case. Ex: /Page, /page, /PAGE.
    $pages = mb_strtolower($pages);
    $request = $this->requestStack->getCurrentRequest();
    // Compare the lowercase path alias (if any) and internal path.
    $path = $this->currentPath->getPath($request);
    // Do not trim a trailing slash if that is the complete path.
    $path = $path === '/' ? $path : rtrim($path, '/');
    $path_alias = mb_strtolower($this->pathAliasManager->getAliasByPath($path));
    $result = $this->pathMatcher->matchPath($path_alias, $pages) || (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));
    return $negate ? !$result : $result;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getConfiguration()->get('enable');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configFactory->get('scroll_progress.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibility() {
    return $this->getConfiguration()->get('visibility') ?: [];
  }

}
