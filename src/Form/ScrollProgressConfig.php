<?php

namespace Drupal\scroll_progress\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Use this class to create configuration form for module.
 */
class ScrollProgressConfig extends ConfigFormBase {

  /**
   * Widget Id.
   */
  public function getFormId() {
    return 'scroll_progress_config';
  }

  /**
   * Create configurations Name.
   */
  protected function getEditableConfigNames() {
    return [
      'scroll_progress.settings',
    ];
  }

  /**
   * Create form for configurations.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('scroll_progress.settings');
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Check this option to enable scroll progress.'),
      '#default_value' => $config->get('enable') ?: FALSE,
    ];
    $form['element'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Element'),
      '#required' => TRUE,
      '#description' => $this->t('CSS selector to configure on which element the scroll progress will be append. Change this settings may require some CSS updates on the site theme and the scroll progress indicator element. If not sure, leave this setting on the default <b>body</b>.'),
      '#default_value' => $config->get('element') ? $config->get('element') : 'body',
    ];
    $form['scroll_progress_theme'] = [
      '#title' => $this->t('Select theme for Scroll to use'),
      '#description' => $this->t('Scroll comes with a lot of themes for progress. Please select the one that you prefer.'),
      '#type' => 'radios',
      '#options' => [
        '1' => $this->t('Straight line'),
        '2' => $this->t('Circular progress'),
        '3' => $this->t('Animated progress'),
        '4' => $this->t('Tooltip progress'),
        '5' => $this->t('Bottom line'),
      ],
      '#default_value' => $config->get('scroll_progress_theme') ? $config->get('scroll_progress_theme') : 1,
    ];
    $form['scroll_progress_color'] = [
      '#title' => $this->t('Color code.'),
      '#description' => $this->t('Default color for scroll progress is #ff0000.'),
      '#type' => 'textfield',
      '#default_value' => $config->get('scroll_progress_color') ? $config->get('scroll_progress_color') : '#ff0000',
    ];

    $form['scroll_progress_load_on_admin_enabled'] = [
      '#title' => $this->t('Load in administration pages.'),
      '#description' => $this->t('SCROLL is disabled by default on administration pages. Check to enable'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('scroll_progress_load_on_admin_enabled') ? $config->get('scroll_progress_load_on_admin_enabled') : 0,
    ];

    $empty = empty($config->get('visibility.request_path.pages'));
    $form['visibility'] = [
      '#type' => 'details',
      '#title' => $this->t('Visibility'),
      '#open' => !$empty,
      '#tree' => TRUE,
    ];
    $form['visibility']['request_path'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Request path'),
      '#tree' => TRUE,
    ];

    $form['visibility']['request_path']['pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#default_value' => $config->get('visibility.request_path.pages') ?: '',
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %user-wildcard for every user page. %front is the front page.", [
        '%user-wildcard' => '/user/*',
        '%front' => '<front>',
      ]),
    ];
    $form['visibility']['request_path']['negate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Negate the condition'),
      '#default_value' => $config->get('visibility.request_path.negate') ?: FALSE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit popup after login configurations.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('scroll_progress.settings')
      ->set('enable', $values['enable'])
      ->set('element', $values['element'])
      ->set('scroll_progress_theme', $values['scroll_progress_theme'])
      ->set('scroll_progress_color', $values['scroll_progress_color'])
      ->set('scroll_progress_load_on_admin_enabled', $values['scroll_progress_load_on_admin_enabled'])
      ->set('visibility', $values['visibility'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
